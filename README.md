# Energo Kalkulator SDK

## Install
```
$ composer req uxf/energo-kalkulator-sdk
```

```php
// bundles.php
return [
    UXFCoreBundle::class => ['all' => true];
    UXFHydratorBundle::class => ['all' => true];
    EnergokalkulatorBundle::class => ['all' => true];
];
```

## Config

```php
// packages/energo_kalkulator.php
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('energo_kalkulator', [
        'base_url' => 'https://xxx.com', // optional
        'api_key' => env('API_KEY'),
    ]);
};
```

## Usage

```php
use EnergoKalkulator\EnergoKalkulatorService;

class MagicService
{
    public function __construct(private readonly EnergoKalkulatorService $service)
    {
    } 

    public function run()
    {
        $data = $service->getGasEstimateWithPrepaid(
            currentPartner: 1,
            currentProduct: 1,
            distributor: 85,
            prepaid: 3333,
        );
    }
}
```
