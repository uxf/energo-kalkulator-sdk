<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Rating;

class RatingCategoryResponse
{
    public function __construct(
        public string $id,
        public string $label,
        public int $value,
    ) {
    }
}
