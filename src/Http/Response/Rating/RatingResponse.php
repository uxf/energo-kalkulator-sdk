<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Rating;

class RatingResponse
{
    /**
     * @param RatingCategoryResponse[] $categories
     */
    public function __construct(
        public string $id,
        public int $total,
        public array $categories,
    ) {
    }
}
