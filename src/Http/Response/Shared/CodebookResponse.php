<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Shared;

final class CodebookResponse
{
    public function __construct(
        public readonly int $id,
        public readonly string $name,
    ) {
    }

    /**
     * @param mixed[] $rawData
     */
    public static function create(array $rawData): self
    {
        return new static((int) $rawData['id'], (string) $rawData['name']);
    }
}
