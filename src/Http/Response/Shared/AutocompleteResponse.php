<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Shared;

class AutocompleteResponse
{
    public function __construct(
        public readonly int $value,
        public readonly string $label,
    ) {
    }
}
