<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Shared;

class FaqResponse
{
    public function __construct(
        public int $id,
        public string $question,
        public string $answerMD,
    ) {
    }
}
