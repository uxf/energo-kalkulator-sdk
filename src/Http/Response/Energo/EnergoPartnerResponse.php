<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Energo;

use EnergoKalkulator\Http\Response\Electricity\ElectricityPartnerResponse;
use EnergoKalkulator\Http\Response\Gas\GasPartnerResponse;
use UXF\Core\Type\DateTime;

class EnergoPartnerResponse
{
    public function __construct(
        public int $id,
        public ?string $name,
        public ?string $descriptionMD,
        public ?ElectricityPartnerResponse $partnerElectro,
        public ?GasPartnerResponse $partnerGas,
        public ?string $title,
        public ?string $metaTitle,
        public ?string $metaDescription,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public ?DateTime $adminLastEdit,
        public ?string $seoName = null,
    ) {
    }
}
