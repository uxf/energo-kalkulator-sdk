<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

class ProductDiscountToInvoiceResponse
{
    public function __construct(
        public int $id,
        public int $discountToInvoice,
    ) {
    }
}
