<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

use EnergoKalkulator\Enum\CooperationMethodEnum;
use EnergoKalkulator\Http\Response\Rating\RatingResponse;
use UXF\Core\Type\DateTime;
use UXF\Storage\Http\Response\FileResponse;

class ElectricityPartnerResponse
{
    public function __construct(
        public int $id,
        public ?int $energoPartnerId,
        public string $name,
        public ?string $descriptionMD,
        public ?string $logoUrl,
        public ?string $logoSquareUrl,
        public ?string $invoiceHelpDistributionRateMD,
        public ?FileResponse $invoiceHelpDistributionRateImage,
        public ?string $invoiceHelpInvoicedPaymentMD,
        public ?FileResponse $invoiceHelpInvoicedPaymentImage,
        public ?string $invoiceHelpHighTariffMD,
        public ?FileResponse $invoiceHelpHighTariffImage,
        public ?string $invoiceHelpLowTariffMD,
        public ?FileResponse $invoiceHelpLowTariffImage,
        public ?string $invoiceHelpBreakerMD,
        public ?FileResponse $invoiceHelpBreakerImage,
        public ?string $invoiceHelpCurrentProductMD,
        public ?FileResponse $invoiceHelpCurrentProductImage,
        public CooperationMethodEnum $cooperationMethod,
        public ?string $termsOfServicePreview,
        public ?string $companyAddress,
        public ?string $contactEmail,
        public ?string $tollFreePhone,
        public ?string $web,
        public ?float $latitude,
        public ?float $longitude,
        public ?string $urlVop,
        public ?string $urlPricelist,
        public ?string $longArticleMD,
        public int $ratingCustomerService,
        public int $ratingProvidedService,
        public int $ratingInformationAccess,
        public int $ratingKarma,
        public int $ratingOnlineSupport,
        public int $ratingTotal,
        public ?RatingResponse $rating,
        public ?int $estimateDefaultProductId,
        public bool $isActive,
        public string $title,
        public string $metaTitle,
        public ?string $metaDescription,
        public bool $supplierFinished,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public ?DateTime $adminLastEdit,
        public ?string $seoName = null,
        public ?string $powerOfAttorney = null,
        public ?string $preContractualInformation = null,
        public ?string $eonId = null,
        public ?string $eonName = null,
        public ?string $defaultProductEonId = null,
        public ?string $defaultProductEonName = null,
    ) {
    }
}
