<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

use EnergoKalkulator\Dto\Electicity\ElectricityCalculationPricesDto;

class ElectricityCalculatorItemResponse
{
    public function __construct(
        public readonly float $maxSaving,
        public readonly bool $entirePeriodSavingIsMax,
        public readonly int $productId,
        public readonly string $partnerName,
        public readonly ElectricityPartnerResponse $partner,
        public readonly ElectricityProductResponse $product,
        public readonly string $productName,
        public readonly float $resultPrice,
        public readonly float $priceCurrent,
        public readonly float $priceExpected,
        public readonly float $priceCalculatedAnnually,
        public readonly float $priceCalculatedMonthly,
        public readonly float $priceCalculatedAnnuallyWithoutDiscountToInvoice,
        public readonly float $priceCalculatedMonthlyWithoutDiscountToInvoice,
        public readonly float $saving,
        public readonly float $savingWithoutDiscountToInvoice,
        public readonly float $savingEntirePeriod,
        public readonly ElectricityCalculationPricesDto $priceExpectedDetail,
        public readonly ElectricityCalculationPricesDto $priceResultDetail,
        public readonly float $discountToInvoice,
        public readonly float $discountToInvoiceWithoutVat,
        public readonly bool $kalkulatorTip,
    ) {
    }
}
