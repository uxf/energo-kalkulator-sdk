<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

use UXF\Core\Type\DateTime;

class ElectricityProductPreviewResponse
{
    public function __construct(
        public int $id,
        public string $name,
        public bool $isActive,
        public ?bool $isActiveForPurchase,
        public ?bool $activeForNewCustomers,
        public ?bool $activeForCurrentCustomers,
        public ?bool $activeForSelectCurrentExact,
        public ?bool $activeForSelectCurrentEstimate,
        public bool $private,
        public ElectricityPartnerPreviewResponse $partner,
        public bool $preferredSale,
        public bool $availableForSale,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public ?DateTime $adminLastEdit,
    ) {
    }
}
