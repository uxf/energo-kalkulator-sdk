<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

use EnergoKalkulator\Enum\ElectricityContractTypeEnum;
use EnergoKalkulator\Enum\MarketingCampaignCodeEnum;
use EnergoKalkulator\Http\Response\Shared\AutocompleteResponse;
use JetBrains\PhpStorm\Deprecated;
use UXF\Core\Type\DateTime;

readonly class ElectricityProductDetailResponse
{
    /**
     * @param string[] $productInfoListBasic
     * @param string[] $productInfoListExtended
     * @param AutocompleteResponse[] $faqs
     */
    public function __construct(
        public int $id,
        public AutocompleteResponse $partner,
        public int $partnerId,
        public string $partnerName,
        public string $name,
        #[Deprecated]
        public bool $guaranteedTariff,
        public bool $partnerTransition,
        public bool $isActive,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public bool $private,
        public bool $preferredSale,
        public bool $availableForSale,
        public int $lengthOfCommitment,
        public int $lengthOfFixation,
        public string $tzbId,
        public ?string $descriptionMD = null,
        public ?string $noteInternal = null,
        public ?DateTime $validFrom = null,
        public ?DateTime $validTo = null,
        public ?bool $isActiveForCurrentCustomers = null,
        public ?bool $isActiveForNewCustomers = null,
        public ?bool $isActiveForPurchase = null,
        public ?bool $isActiveForSelectCurrentExact = null,
        public ?bool $isActiveForSelectCurrentEstimate = null,
        public ?string $tzbName = null,
        public ?DateTime $tzbValidFrom = null,
        public ?DateTime $tzbValidTo = null,
        public array $productInfoListBasic = [],
        public array $productInfoListExtended = [],
        public ?int $followingProductId = null,
        public array $faqs = [],
        public ?string $shortName = null,
        public ?string $gift = null,
        public ?string $bonus = null,
        public ?DateTime $fixedEndOfCommitment = null,
        public ?DateTime $fixedEndOfFixation = null,
        public ?string $label = null,
        public ?string $labelTooltip = null,
        public ?string $productCode = null,
        public ?DateTime $adminLastEdit = null,
        public ?ElectricityContractTypeEnum $contractType = null,
        public bool $startsWithSignature = false,
        public ?string $contractPdf = null,
        public ?MarketingCampaignCodeEnum $marketingCampaignCode = null,
        public ?string $eonId = null,
        public ?string $eonName = null,
        public ?string $productVersion = null,
    ) {
    }
}
