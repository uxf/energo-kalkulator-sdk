<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

use EnergoKalkulator\Http\Response\Shared\AutocompleteResponse;
use UXF\Core\Type\Date;

class ElectricityPriceProductResponse
{
    public function __construct(
        public int $id,
        public ?string $tzbId,
        public ?string $tzbPricelistId,
        public ?Date $validTo,
        public Date $validFrom,
        public float $priceMonthly,
        public float $priceMWhHighTariff,
        public ?float $priceMWhLowTariff,
        public AutocompleteResponse $distributionRate,
        public AutocompleteResponse $distributor,
        public ElectricityProductPreviewResponse $product,
    ) {
    }
}
