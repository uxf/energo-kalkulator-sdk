<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

use EnergoKalkulator\Enum\CooperationMethodEnum;
use UXF\Core\Type\DateTime;
use UXF\Storage\Http\Response\FileResponse;

class ElectricityPartnerPreviewResponse
{
    public function __construct(
        public int $id,
        public ?int $energoPartnerId,
        public string $name,
        public ?string $seoName,
        public ?string $logoUrl,
        public ?string $logoSquareUrl,
        public ?FileResponse $logo,
        public ?FileResponse $logoSquare,
        public CooperationMethodEnum $cooperationMethod,
        public ?string $companyAddress,
        public ?string $contactEmail,
        public ?string $tollFreePhone,
        public ?float $longitude,
        public ?float $latitude,
        public ?string $web,
        public ?string $urlVop,
        public ?string $urlPricelist,
        public ?string $longArticleMD,
        public bool $active,
        public ?int $estimateDefaultProductId,
        public bool $supplierFinished,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public ?DateTime $adminLastEdit,
        public ?int $xorder = null,
    ) {
    }
}
