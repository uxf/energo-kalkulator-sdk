<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

class ElectricityCalculatorCommonResponse
{
    public function __construct(
        public float $priceCurrentAnnually,
        public float $priceCurrentMonthly,
        public float $consumptionHT,
        public float $consumptionLT,
    ) {
    }
}
