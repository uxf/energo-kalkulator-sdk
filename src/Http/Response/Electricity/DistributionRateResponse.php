<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

class DistributionRateResponse
{
    public function __construct(
        public int $id,
        public string $name,
        public string $description,
        public bool $lowTariff,
    ) {
    }
}
