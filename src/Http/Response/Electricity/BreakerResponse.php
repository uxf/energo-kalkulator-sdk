<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

class BreakerResponse
{
    public function __construct(
        public int $id,
        public string $name,
        public bool $overLimitPayment,
        public int $breaker,
        public int $phases,
    ) {
    }
}
