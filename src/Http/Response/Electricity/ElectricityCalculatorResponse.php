<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Electricity;

class ElectricityCalculatorResponse
{
    /**
     * @param ElectricityCalculatorItemResponse[] $calculatedItems
     */
    public function __construct(
        public array $calculatedItems,
        public ElectricityCalculatorCommonResponse $common,
    ) {
    }
}
