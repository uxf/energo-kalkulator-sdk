<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

class GasCalculatorResponse
{
    /**
     * @param GasCalculatorItemResponse[] $calculatedItems
     */
    public function __construct(
        public array $calculatedItems,
        public GasCalculatorCommonResponse $common,
    ) {
    }
}
