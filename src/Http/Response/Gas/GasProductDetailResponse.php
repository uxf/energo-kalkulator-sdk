<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

use EnergoKalkulator\Enum\GasContractTypeEnum;
use EnergoKalkulator\Enum\MarketingCampaignCodeEnum;
use EnergoKalkulator\Http\Response\Shared\AutocompleteResponse;
use JetBrains\PhpStorm\Deprecated;
use UXF\Core\Type\DateTime;

readonly class GasProductDetailResponse
{
    /**
     * @param string[] $productInfoListBasic
     * @param string[] $productInfoListExtended
     * @param AutocompleteResponse[] $faqs
     */
    public function __construct(
        public int $id,
        public AutocompleteResponse $partner,
        public int $partnerId,
        public string $name,
        public ?string $descriptionMD,
        public ?string $noteInternal,
        public ?DateTime $validFrom,
        public ?DateTime $validTo,
        #[Deprecated]
        public bool $guaranteedTariff,
        public bool $partnerTransition,
        public bool $isActive,
        public ?bool $isActiveForNewCustomers,
        public ?bool $isActiveForCurrentCustomers,
        public ?bool $isActiveForPurchase,
        public ?bool $isActiveForSelectCurrentExact,
        public ?bool $isActiveForSelectCurrentEstimate,
        public bool $private,
        public bool $preferredSale,
        public bool $availableForSale,
        public ?string $label,
        public ?string $labelTooltip,
        public int $lengthOfCommitment,
        public int $lengthOfFixation,
        public string $tzbId,
        public ?string $tzbName,
        public ?DateTime $tzbValidFrom,
        public ?DateTime $tzbValidTo,
        public array $productInfoListBasic,
        public array $productInfoListExtended,
        public ?int $followingProductId,
        public array $faqs,
        public ?string $shortName,
        public ?string $gift,
        public ?string $bonus,
        public ?DateTime $fixedEndOfCommitment,
        public ?DateTime $fixedEndOfFixation,
        public ?string $productCode,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public ?DateTime $adminLastEdit,
        public ?GasContractTypeEnum $contractType,
        public bool $startsWithSignature = false,
        public ?string $contractPdf = null,
        public ?MarketingCampaignCodeEnum $marketingCampaignCode = null,
        public ?string $eonId = null,
        public ?string $eonName = null,
        public ?string $productVersion = null,
    ) {
    }
}
