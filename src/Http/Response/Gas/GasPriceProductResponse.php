<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

use EnergoKalkulator\Http\Response\Shared\AutocompleteResponse;
use UXF\Core\Type\Date;

class GasPriceProductResponse
{
    public function __construct(
        public int $id,
        public ?string $tzbId,
        public ?Date $validTo,
        public Date $validFrom,
        public ?float $pricePerM3,
        public ?string $tzbPricelistId,
        public AutocompleteResponse $odberovePasmo,
        public float $priceMonthly,
        public float $priceMWh,
        public GasProductPreviewResponse $product,
    ) {
    }
}
