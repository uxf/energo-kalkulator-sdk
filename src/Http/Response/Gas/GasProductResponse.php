<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

use EnergoKalkulator\Enum\GasContractTypeEnum;
use EnergoKalkulator\Enum\MarketingCampaignCodeEnum;
use EnergoKalkulator\Http\Response\Shared\FaqResponse;
use JetBrains\PhpStorm\Deprecated;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

readonly class GasProductResponse
{
    /**
     * @param string[] $productInfoListBasic
     * @param string[] $productInfoListExtended
     * @param GasProductDiscountToInvoiceResponse[] $discountToInvoices
     * @param FaqResponse[] $faqs
     */
    public function __construct(
        public int $id,
        public string $name,
        public ?string $descriptionMD,
        public ?DateTime $validFrom,
        public ?DateTime $validTo,
        #[Deprecated]
        public bool $guaranteedTariff,
        public bool $partnerTransition,
        public bool $isActive,
        public ?bool $isActiveForPurchase,
        public ?bool $isActiveForNewCustomers,
        public ?bool $isActiveForCurrentCustomers,
        public ?bool $isActiveForSelectCurrentEstimate,
        public ?bool $isActiveForSelectCurrentExact,
        public bool $private,
        public int $lengthOfFixation,
        public int $lengthOfCommitment,
        public array $productInfoListBasic,
        public array $productInfoListExtended,
        public array $faqs,
        public ?int $followingProductId,
        public ?string $followingProductName,
        public ?Date $followingProductFixedEndOfCommitment,
        public ?Date $followingProductFixedEndOfFixation,
        public ?int $followingProductLengthOfCommitment,
        public ?int $followingProductLengthOfFixation,
        public ?string $shortName,
        public ?string $gift,
        public ?string $bonus,
        public GasPartnerResponse $partner,
        public ?DateTime $fixedEndOfCommitment,
        public ?DateTime $fixedEndOfFixation,
        public ?string $label,
        public ?string $labelTooltip,
        public array $discountToInvoices,
        public bool $preferredSale,
        public bool $availableForSale,
        public ?string $productCode,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public ?DateTime $adminLastEdit,
        public ?GasContractTypeEnum $contractType,
        public bool $startsWithSignature = false,
        public ?string $contractPdf = null,
        public ?MarketingCampaignCodeEnum $marketingCampaignCode = null,
        public ?string $eonId = null,
        public ?string $eonName = null,
        public ?string $productVersion = null,
    ) {
    }
}
