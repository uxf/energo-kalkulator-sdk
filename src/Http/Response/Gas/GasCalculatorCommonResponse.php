<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

class GasCalculatorCommonResponse
{
    public function __construct(
        public float $priceCurrentAnnually,
        public float $priceCurrentMonthly,
        public float $consumption,
    ) {
    }

    public static function create(
        float $priceCurrentAnnually,
        float $priceCurrentMonthly,
        float $consumption,
    ): self {
        return new self(
            round($priceCurrentAnnually, 2),
            round($priceCurrentMonthly, 2),
            round($consumption, 4),
        );
    }
}
