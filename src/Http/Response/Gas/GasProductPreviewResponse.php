<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

use UXF\Core\Type\DateTime;

class GasProductPreviewResponse
{
    public function __construct(
        public int $id,
        public string $name,
        public bool $isActive,
        public ?bool $isActiveForPurchase,
        public ?bool $activeForNewCustomers,
        public ?bool $activeForCurrentCustomers,
        public ?bool $activeForSelectCurrentExact,
        public ?bool $activeForSelectCurrentEstimate,
        public bool $private,
        public GasPartnerPreviewResponse $partner,
        public bool $preferredSale,
        public bool $availableForSale,
        public DateTime $createdAt,
        public DateTime $updatedAt,
        public ?DateTime $adminLastEdit,
    ) {
    }
}
