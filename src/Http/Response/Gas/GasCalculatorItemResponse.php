<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

use EnergoKalkulator\Dto\Gas\GasCalculationPricesDto;

class GasCalculatorItemResponse
{
    public float $maxSaving;
    public bool $entirePeriodSavingIsMax;

    public function __construct(
        public int $productId,
        public string $partnerName,
        public GasPartnerResponse $partner,
        public GasProductResponse $product,
        public string $productName,
        public float $resultPrice,
        public float $priceCurrent,
        public float $priceExpected,
        public float $priceCalculatedAnnually,
        public float $priceCalculatedMonthly,
        public float $priceCalculatedAnnuallyWithoutDiscountToInvoice,
        public float $priceCalculatedMonthlyWithoutDiscountToInvoice,
        public float $saving,
        public float $savingWithoutDiscountToInvoice,
        public float $savingEntirePeriod,
        public GasCalculationPricesDto $priceExpectedDetail,
        public GasCalculationPricesDto $priceResultDetail,
        public float $discountToInvoice,
        public float $discountToInvoiceWithoutVat,
        public bool $kalkulatorTip,
    ) {
        $this->maxSaving = max($saving, $savingEntirePeriod);
        $this->entirePeriodSavingIsMax = $savingEntirePeriod >= $saving;
    }
}
