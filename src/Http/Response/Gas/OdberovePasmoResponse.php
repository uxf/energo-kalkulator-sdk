<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

class OdberovePasmoResponse
{
    public function __construct(
        public int $id,
        public string $name,
        public string $description,
        public float $consumptionFrom,
        public float $consumptionTo,
    ) {
    }
}
