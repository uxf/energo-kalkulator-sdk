<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Response\Gas;

class GasProductDiscountToInvoiceResponse
{
    public function __construct(
        public int $id,
        public int $discountToInvoice,
    ) {
    }
}
