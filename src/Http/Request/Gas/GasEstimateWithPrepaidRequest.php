<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Gas;

class GasEstimateWithPrepaidRequest
{
    /**
     * @param int[]|null $filterProducts
     */
    public function __construct(
        public ?int $distributor = null,
        public ?int $smartAddressId = null,
        public float $prepaid = 0.0,
        public ?int $currentProduct = null,
        public ?int $currentPartner = null,
        public ?array $filterProducts = null,
        public bool $orderByMaxSaving = false,
    ) {
    }
}
