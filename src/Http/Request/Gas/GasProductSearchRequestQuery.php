<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Gas;

class GasProductSearchRequestQuery
{
    public function __construct(
        public readonly ?int $partner = null,
        public readonly ?string $name = null,
    ) {
    }
}
