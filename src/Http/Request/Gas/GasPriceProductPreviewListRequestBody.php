<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Gas;

class GasPriceProductPreviewListRequestBody
{
    public function __construct(
        public readonly int $odberovePasmo,
    ) {
    }
}
