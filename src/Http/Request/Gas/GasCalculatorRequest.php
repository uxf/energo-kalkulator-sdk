<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Gas;

use Symfony\Component\Validator\Constraints as Assert;

class GasCalculatorRequest
{
    /**
     * @param int[]|null $filterProducts
     * @param int[]|null $filterPartners
     */
    public function __construct(
        #[Assert\Range(min: 0.001, max: 630)]
        public float $consumptionMWh,
        public ?int $distributor = null,
        public ?int $smartAddressId = null,
        public ?float $lastInvoicedPrice = null,
        public ?int $currentProduct = null,
        public ?int $currentPartner = null,
        public ?array $filterProducts = null,
        public ?array $filterPartners = null,
        public bool $displayPrivate = false,
        public ?string $currentProductName = null,
        public ?int $forceDiscountToInvoice = null,
        public bool $orderByMaxSaving = false,
    ) {
    }
}
