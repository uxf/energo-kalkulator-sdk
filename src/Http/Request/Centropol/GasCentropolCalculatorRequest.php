<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Centropol;

class GasCentropolCalculatorRequest
{
    /**
     * @param int[]|null $filterProducts
     * @param int[]|null $filterPartners
     */
    public function __construct(
        public readonly float $consumptionMWh,
        public readonly ?int $distributor = null,
        public readonly ?int $smartAddress = null,
        public readonly ?float $lastInvoicedPrice = null,
        public readonly ?int $currentProduct = null,
        public readonly ?int $currentPartner = null,
        /** @var int[]|null $filterProducts */
        public readonly ?array $filterProducts = null,
        /** @var int[]|null $filterPartners */
        public readonly ?array $filterPartners = null,
        public readonly bool $displayPrivate = false,
        public readonly ?string $currentProductName = null,
        public readonly ?int $forceDiscountToInvoice = null,
        public readonly ?string $address = null,
    ) {
    }
}
