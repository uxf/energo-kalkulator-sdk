<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Electricity;

class ElectricityProductSearchRequestQuery
{
    public function __construct(
        public readonly ?int $partner = null,
        public readonly ?string $name = null,
    ) {
    }
}
