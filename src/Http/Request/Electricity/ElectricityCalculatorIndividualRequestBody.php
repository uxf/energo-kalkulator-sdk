<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Electricity;

class ElectricityCalculatorIndividualRequestBody
{
    /**
     * @param int[]|null $filterProducts
     * @param int[]|null $filterPartners
     */
    public function __construct(
        public readonly float $individualPriceExpectedMonthly,
        public readonly float $individualPriceExpectedMWhHighTariff,
        public readonly ?float $individualPriceExpectedMWhLowTariff,
        public readonly int $distributionRate,
        public readonly int $breaker,
        public readonly float $consumptionHighTariff,
        public readonly float $consumptionLowTariff,
        /** @deprecated */
        public readonly ?int $distributor = null,
        public readonly ?int $smartAddressId = null,
        public readonly ?int $breakerValueAmper = null,
        public readonly ?float $lastInvoicedPrice = null,
        public readonly ?int $currentProduct = null,
        public readonly ?int $currentPartner = null,
        public readonly ?array $filterProducts = null,
        public readonly ?array $filterPartners = null,
        public readonly bool $displayPrivate = false,
        public readonly ?string $currentProductName = null,
        public readonly ?int $forceDiscountToInvoice = null,
        public readonly bool $orderByMaxSaving = false,
    ) {
    }
}
