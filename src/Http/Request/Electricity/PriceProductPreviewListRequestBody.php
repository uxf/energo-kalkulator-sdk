<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Electricity;

class PriceProductPreviewListRequestBody
{
    public function __construct(
        public readonly int $distributionRate,
        public readonly ?int $distributor = null,
        public readonly ?int $smartAddressId = null,
    ) {
    }
}
