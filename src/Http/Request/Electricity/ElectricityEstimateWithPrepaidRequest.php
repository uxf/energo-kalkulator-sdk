<?php

declare(strict_types=1);

namespace EnergoKalkulator\Http\Request\Electricity;

class ElectricityEstimateWithPrepaidRequest
{
    /**
     * @param int[]|null $filterProducts
     */
    public function __construct(
        public int $buildingType,
        /** @deprecated */
        public ?int $distributor = null,
        public ?int $smartAddressId = null,
        public float $prepaid = 0.0,
        public bool $usageCommon = false,
        public bool $usageWaterHeating = false,
        public bool $usageHeating = false,
        public ?int $currentPartner = null,
        public ?int $currentProduct = null,
        public ?array $filterProducts = null,
        public bool $orderByMaxSaving = false,
    ) {
    }
}
