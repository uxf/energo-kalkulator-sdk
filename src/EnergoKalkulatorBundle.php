<?php

declare(strict_types=1);

namespace EnergoKalkulator;

use EnergoKalkulator\Client\HttpClient;
use EnergoKalkulator\Client\HttpEnergoKalkulatorService;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class EnergoKalkulatorBundle extends AbstractBundle
{
    protected string $extensionAlias = 'energo_kalkulator';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
            ->scalarNode('base_url')->defaultValue('https://energokalkulator.uxf.cz')->end()
            ->scalarNode('api_key')->isRequired()
            ->end()
            ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $services = $container->services();

        $services->set(EnergoKalkulatorService::class, HttpEnergoKalkulatorService::class)
            ->autowire();

        $services->set(HttpClient::class)
            ->arg('$baseUrl', $config['base_url'])
            ->arg('$apiKey', $config['api_key'])
            ->autowire();
    }
}
