<?php

declare(strict_types=1);

namespace EnergoKalkulator\Exception;

use Exception;
use Throwable;

class EnergokalkulatorException extends Exception
{
    public function __construct(
        string $errorMessage,
        ?Throwable $previous = null,
    ) {
        parent::__construct($errorMessage, 0, $previous);
    }
}
