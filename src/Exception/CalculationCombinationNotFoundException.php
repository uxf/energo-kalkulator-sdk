<?php

declare(strict_types=1);

namespace EnergoKalkulator\Exception;

use Exception;

final class CalculationCombinationNotFoundException extends Exception
{
    public function __construct()
    {
        parent::__construct('api.energoKalkulator.combinationNotFound');
    }
}
