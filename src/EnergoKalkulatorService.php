<?php

declare(strict_types=1);

namespace EnergoKalkulator;

use EnergoKalkulator\Http\Request\Electricity\ElectricityCalculatorIndividualRequestBody;
use EnergoKalkulator\Http\Request\Electricity\ElectricityCalculatorRequest;
use EnergoKalkulator\Http\Request\Electricity\ElectricityEstimateWithoutPrepaidRequest;
use EnergoKalkulator\Http\Request\Electricity\ElectricityEstimateWithPrepaidRequest;
use EnergoKalkulator\Http\Request\Electricity\PriceProductPreviewListRequestBody;
use EnergoKalkulator\Http\Request\Gas\GasCalculatorIndividualRequestBody;
use EnergoKalkulator\Http\Request\Gas\GasCalculatorRequest;
use EnergoKalkulator\Http\Request\Gas\GasEstimateWithoutPrepaidRequest;
use EnergoKalkulator\Http\Request\Gas\GasEstimateWithPrepaidRequest;
use EnergoKalkulator\Http\Request\Gas\GasPriceProductPreviewListRequestBody;
use EnergoKalkulator\Http\Response\Electricity\BreakerResponse;
use EnergoKalkulator\Http\Response\Electricity\DistributionRateResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityCalculatorResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityPartnerResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityPriceProductResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityProductDetailResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityProductResponse;
use EnergoKalkulator\Http\Response\Energo\EnergoPartnerResponse;
use EnergoKalkulator\Http\Response\Gas\GasCalculatorResponse;
use EnergoKalkulator\Http\Response\Gas\GasPartnerResponse;
use EnergoKalkulator\Http\Response\Gas\GasPriceProductResponse;
use EnergoKalkulator\Http\Response\Gas\GasProductDetailResponse;
use EnergoKalkulator\Http\Response\Gas\GasProductResponse;
use EnergoKalkulator\Http\Response\Shared\CodebookResponse;

interface EnergoKalkulatorService
{
    /**
     * @return ElectricityPartnerResponse[]
     */
    public function getPartners(): array;

    /**
     * @return GasPartnerResponse[]
     */
    public function getGasPartners(): array;

    /**
     * @return DistributionRateResponse[]
     */
    public function getDistributionRates(): array;

    /**
     * @return CodebookResponse[]
     */
    public function getFloorAreaRanges(): array;

    /**
     * @return CodebookResponse[]
     */
    public function getPersonsCountRanges(): array;

    /**
     * @return CodebookResponse[]
     */
    public function getIsolations(): array;

    /**
     * @return BreakerResponse[]
     */
    public function getBreakers(): array;

    /**
     * @return EnergoPartnerResponse[]
     */
    public function getEnergoPartners(): array;

    public function getEnergoPartner(int $id): EnergoPartnerResponse;

    public function getAdminGasCalculationResult(
        GasCalculatorIndividualRequestBody | GasCalculatorRequest $input,
    ): GasCalculatorResponse;

    public function getAdminElectricityCalculationResult(
        ElectricityCalculatorIndividualRequestBody | ElectricityCalculatorRequest $input,
    ): ElectricityCalculatorResponse;

    public function getElectricityProduct(int $id): ElectricityProductDetailResponse;

    /**
     * @return ElectricityProductResponse[]
     */
    public function getElectricityProducts(int $partnerId): array;

    /**
     * @return GasProductResponse[]
     */
    public function getGasProducts(int $partnerId): array;

    public function getGasProduct(int $id): GasProductDetailResponse;

    /**
     * @return ElectricityPriceProductResponse[]
     */
    public function getElectricityPricePreview(PriceProductPreviewListRequestBody $input): array;

    /**
     * @return GasPriceProductResponse[]
     */
    public function getGasPricePreview(GasPriceProductPreviewListRequestBody $input): array;

    public function getElectricityCalculationIndividual(
        ElectricityCalculatorIndividualRequestBody $input,
    ): ElectricityCalculatorResponse;

    public function getElectricityCalculation(
        ElectricityCalculatorRequest $input,
    ): ElectricityCalculatorResponse;

    public function getElectricityEstimationWithPrepaid(
        ElectricityEstimateWithPrepaidRequest $input,
    ): ElectricityCalculatorResponse;

    public function getElectricityEstimationWithoutPrepaid(
        ElectricityEstimateWithoutPrepaidRequest $input,
    ): ElectricityCalculatorResponse;

    public function getGasCalculationIndividual(GasCalculatorIndividualRequestBody $input): GasCalculatorResponse;

    public function getGasCalculation(GasCalculatorRequest $input): GasCalculatorResponse;

    public function getGasEstimateWithPrepaid(GasEstimateWithPrepaidRequest $input): GasCalculatorResponse;

    public function getGasEstimateWithoutPrepaid(GasEstimateWithoutPrepaidRequest $input): GasCalculatorResponse;
}
