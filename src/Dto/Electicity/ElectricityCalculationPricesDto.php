<?php

declare(strict_types=1);

namespace EnergoKalkulator\Dto\Electicity;

class ElectricityCalculationPricesDto
{
    public function __construct(
        public float $priceResult,
        public float $priceDistributionMonthly,
        public float $priceDistributionMWhHT,
        public float $priceDistributionMWhLT,
        public float $priceDistrinutionMWhHT,
        public float $priceDistrinutionMWhLT,
        public float $priceProductMonthly,
        public float $priceProductMWhHT,
        public float $priceProductMWhLT,
        public float $yearlyDistributionPrice,
        public float $yearlyPartnerPrice,
    ) {
    }

    public static function create(
        float $priceResult,
        float $priceDistributionMonthly,
        float $priceDistributionMWhHT,
        float $priceDistributionMWhLT,
        float $priceProductMonthly,
        float $priceProductMWhHT,
        float $priceProductMWhLT,
        float $yearlyDistributionPrice,
        float $yearlyPartnerPrice,
    ): self {
        return new self(
            round($priceResult, 2),
            round($priceDistributionMonthly, 2),
            round($priceDistributionMWhHT, 2),
            round($priceDistributionMWhLT, 2),
            round($priceDistributionMWhHT, 2),
            round($priceDistributionMWhLT, 2),
            round($priceProductMonthly, 2),
            round($priceProductMWhHT, 2),
            round($priceProductMWhLT, 2),
            round($yearlyDistributionPrice, 2),
            $yearlyPartnerPrice,
        );
    }
}
