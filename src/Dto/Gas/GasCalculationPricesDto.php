<?php

declare(strict_types=1);

namespace EnergoKalkulator\Dto\Gas;

class GasCalculationPricesDto
{
    public function __construct(
        public float $priceResult,
        public float $priceDistributionMonthly,
        public float $priceDistributionMWh,
        public float $priceDistrinutionMWh,
        public float $priceProductMonthly,
        public float $priceProductMWh,
        public float $yearlyDistributionPrice,
        public float $yearlyPartnerPrice,
    ) {
    }

    public static function create(
        float $priceResult,
        float $priceDistributionMonthly,
        float $priceDistributionMWh,
        float $priceProductMonthly,
        float $priceProductMWh,
        float $yearlyDistributionPrice,
        float $yearlyPartnerPrice,
    ): self {
        return new self(
            round($priceResult, 2),
            round($priceDistributionMonthly, 2),
            round($priceDistributionMWh, 2),
            round($priceDistributionMWh, 2),
            round($priceProductMonthly, 2),
            round($priceProductMWh, 2),
            round($yearlyDistributionPrice, 2),
            round($yearlyPartnerPrice, 2),
        );
    }
}
