<?php

declare(strict_types=1);

namespace EnergoKalkulator\Enum;

enum GasContractTypeEnum: string
{
    case GAS_CENTROPOL_VYCHOZI = 'GAS_CENTROPOL_VYCHOZI';
    case GAS_CENTROPOL_FIXNE_BEZ_ZAVAZKU = 'GAS_CENTROPOL_FIXNE_BEZ_ZAVAZKU';
    case GAS_CENTROPOL_ZAVAZKU_PRO_ZACATEK = 'GAS_CENTROPOL_ZAVAZKU_PRO_ZACATEK';
    case GAS_CENTROPOL_STRATEGICKY_NA_2_ROKY_PRO_ZACATEK = 'GAS_CENTROPOL_STRATEGICKY_NA_2_ROKY_PRO_ZACATEK';
    case GAS_CENTROPOL_FIXNE_ONLINE = 'GAS_CENTROPOL_FIXNE_ONLINE';
    case GAS_CENTROPOL_FIXNE_OD_PODPISU_2_ROKY = 'GAS_CENTROPOL_FIXNE_OD_PODPISU_2_ROKY';
    case GAS_CENTROPOL_BEZ_ZAVAZKU_SE_STROPEM = 'GAS_CENTROPOL_BEZ_ZAVAZKU_SE_STROPEM';
    case GAS_CENTROPOL_FIXNE_NA_1_A_PUL_ROKU = 'GAS_CENTROPOL_FIXNE_NA_1_A_PUL_ROKU';
    case GAS_CENTROPOL_BEZ_ZAVAZKU = 'GAS_CENTROPOL_BEZ_ZAVAZKU';

    /**
     * @return array<string, string>
     */
    public static function getOptions(): array
    {
        return [
            self::GAS_CENTROPOL_VYCHOZI->value => 'Centropol - výchozí smlouva',
            self::GAS_CENTROPOL_FIXNE_BEZ_ZAVAZKU->value => 'Centropol - fixně bez závazku',
            self::GAS_CENTROPOL_ZAVAZKU_PRO_ZACATEK->value => 'Centropol - bez závazku pro začátek',
            self::GAS_CENTROPOL_STRATEGICKY_NA_2_ROKY_PRO_ZACATEK->value
            => 'Centropol- strategicky na 2 roky pro začátek',
            self::GAS_CENTROPOL_FIXNE_ONLINE->value => 'Centropol- fixně online',
            self::GAS_CENTROPOL_FIXNE_OD_PODPISU_2_ROKY->value => 'Centropol- Fixně od podpisu 2 roky',
            self::GAS_CENTROPOL_BEZ_ZAVAZKU_SE_STROPEM->value => 'Centropol- bez závazku se stropem',
            self::GAS_CENTROPOL_BEZ_ZAVAZKU->value => 'Centropol- bez závazku',
            self::GAS_CENTROPOL_FIXNE_NA_1_A_PUL_ROKU->value => 'Centropol- fixně na 1,5 roku',
        ];
    }
}
