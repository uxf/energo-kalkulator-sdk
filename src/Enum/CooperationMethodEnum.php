<?php

declare(strict_types=1);

namespace EnergoKalkulator\Enum;

enum CooperationMethodEnum: string
{
    case COOPERATING = 'COOPERATING';
    case NOT_COOPERATING = 'NOT_COOPERATING';
}
