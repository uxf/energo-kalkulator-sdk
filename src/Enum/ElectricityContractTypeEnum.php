<?php

declare(strict_types=1);

namespace EnergoKalkulator\Enum;

enum ElectricityContractTypeEnum: string
{
    case ELECTRICITY_CENTROPOL_VYCHOZI = 'ELECTRICITY_CENTROPOL_VYCHOZI';
    case ELECTRICITY_CENTROPOL_MINI = 'ELECTRICITY_CENTROPOL_MINI';
    case ELECTRICITY_CENTROPOL_MINI_NA_DVA_ROKY = 'ELECTRICITY_CENTROPOL_MINI_NA_DVA_ROKY';
    case ELECTRICITY_CENTROPOL_FIXNE_BEZ_ZAVAZKU = 'ELECTRICITY_CENTROPOL_FIXNE_BEZ_ZAVAZKU';
    case ELECTRICITY_CENTROPOL_ZAVAZKU_PRO_ZACATEK = 'ELECTRICITY_CENTROPOL_ZAVAZKU_PRO_ZACATEK';
    case ELECTRICITY_CENTROPOL_STRATEGICKY_NA_2_ROKY_PRO_ZACATEK = 'ELECTRICITY_CENTROPOL_STRATEGICKY_NA_2_ROKY_PRO_ZACATEK';
    case ELECTRICITY_CENTROPOL_FIXNE_ONLINE = 'ELECTRICITY_CENTROPOL_FIXNE_ONLINE';
    case ELECTRICITY_CENTROPOL_FIXNE_OD_PODPISU_2_ROKY = 'ELECTRICITY_CENTROPOL_FIXNE_OD_PODPISU_2_ROKY';
    case ELECTRICITY_CENTROPOL_BEZ_ZAVAZKU_SE_STROPEM = 'ELECTRICITY_CENTROPOL_BEZ_ZAVAZKU_SE_STROPEM';
    case ELECTRICITY_CENTROPOL_FIXNE_NA_1_A_PUL_ROKU = 'ELECTRICITY_CENTROPOL_FIXNE_NA_1_A_PUL_ROKU';

    /**
     * @return array<string, string>
     */
    public static function getOptions(): array
    {
        return [
            self::ELECTRICITY_CENTROPOL_VYCHOZI->value => 'Centropol - výchozí smlouva',
            self::ELECTRICITY_CENTROPOL_MINI->value => 'Centropol - smlouva MINI',
            self::ELECTRICITY_CENTROPOL_MINI_NA_DVA_ROKY->value => 'Centropol - smlouva MINI na dva roky',
            self::ELECTRICITY_CENTROPOL_FIXNE_BEZ_ZAVAZKU->value => 'Centropol - fixně bez závazku',
            self::ELECTRICITY_CENTROPOL_ZAVAZKU_PRO_ZACATEK->value => 'Centropol - bez závazku pro začátek',
            self::ELECTRICITY_CENTROPOL_STRATEGICKY_NA_2_ROKY_PRO_ZACATEK->value
            => 'Centropol- strategicky na 2 roky pro začátek',
            self::ELECTRICITY_CENTROPOL_FIXNE_ONLINE->value => 'Centropol- fixně online',
            self::ELECTRICITY_CENTROPOL_FIXNE_OD_PODPISU_2_ROKY->value => 'Centropol- Fixně od podpisu 2 roky',
            self::ELECTRICITY_CENTROPOL_BEZ_ZAVAZKU_SE_STROPEM->value => 'Centropol- bez závazku se stropem',
            self::ELECTRICITY_CENTROPOL_FIXNE_NA_1_A_PUL_ROKU->value => 'Centropol- fixně na 1,5 roku',
        ];
    }
}
