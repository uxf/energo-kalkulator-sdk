<?php

declare(strict_types=1);

namespace EnergoKalkulator\Enum;

use UXF\CMS\Attribute\CmsLabel;

enum MarketingCampaignCodeEnum: string
{
    #[CmsLabel('200 Kč do FA (SLF200MF)')]
    case CENTROPOL_SLF200MF = 'SLF200MF';

    #[CmsLabel('300 Kč do FA (SLF300MF)')]
    case CENTROPOL_SLF300MF = 'SLF300MF';

    #[CmsLabel('500 Kč do FA (SLF500MF)')]
    case CENTROPOL_SLF500MF = 'SLF500MF';

    #[CmsLabel('700 Kč do FA (SLF700MF)')]
    case CENTROPOL_SLF700MF = 'SLF700MF';

    #[CmsLabel('800 Kč do FA (SLF800MF)')]
    case CENTROPOL_SLF800MF = 'SLF800MF';

    #[CmsLabel('1000 Kč do FA (SLF1000MF)')]
    case CENTROPOL_SLF1000MF = 'SLF1000MF';

    #[CmsLabel('1200 Kč do FA (SLF1200MF)')]
    case CENTROPOL_SLF1200MF = 'SLF1200MF';

    #[CmsLabel('1500 Kč do FA (SLF1500MF)')]
    case CENTROPOL_SLF1500MF = 'SLF1500MF';

    #[CmsLabel('2000 Kč do FA (SLF2000MF)')]
    case CENTROPOL_SLF2000MF = 'SLF2000MF';

    #[CmsLabel('2500 Kč do FA (SLF2500MF)')]
    case CENTROPOL_SLF2500MF = 'SLF2500MF';

    #[CmsLabel('4% z MWh a fixu (SLP4)')]
    case CENTROPOL_SLP4_MF = 'SLP4%MF';

    #[CmsLabel('5% z MWh a fixu (SLP5)')]
    case CENTROPOL_SLP5_MF = 'SLP5%MF';

    #[CmsLabel('7% z MWh a fixu (SLP7)')]
    case CENTROPOL_SLP7_MF = 'SLP7%MF';

    #[CmsLabel('8% z MWh a fixu (SLP8)')]
    case CENTROPOL_SLP8_MF = 'SLP8%MF';

    #[CmsLabel('10% z MWh a fixu (SLP10)')]
    case CENTROPOL_SLP10_MF = 'SLP10%MF';

    #[CmsLabel('15% z MWh a fixu (SLP15)')]
    case CENTROPOL_SLP15_MF = 'SLP15%MF';

    #[CmsLabel('7% z MWh (SLP7PM)')]
    case CENTROPOL_SLP7_PM = 'SLP7PM';

    #[CmsLabel('10% z MWh (SLP10PM)')]
    case CENTROPOL_SLP10_PM = 'SLP10PM';

    #[CmsLabel('15% z MWh (SLP15PM)')]
    case CENTROPOL_SLP15_PM = 'SLP15PM';
}
