<?php

declare(strict_types=1);

namespace EnergoKalkulator\Client;

use EnergoKalkulator\Exception\EnergokalkulatorException;
use Nette\Utils\Json;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;

class HttpClient
{
    /** @var array<string, mixed> */
    private array $cache = [];

    public function __construct(
        private readonly string $baseUrl,
        private readonly string $apiKey,
        private readonly ClientInterface $httpClient,
        private readonly RequestFactoryInterface $requestFactory,
        private readonly StreamFactoryInterface $streamFactory,
    ) {
    }

    /**
     * @return mixed[]
     */
    public function post(string $url, ?object $body): array
    {
        $response = $this->createRequest(
            'POST',
            $url,
            $body !== null
                ? Json::encode($body)
                : null,
        )->getBody()->getContents();

        return $response === ''
            ? []
            : Json::decode($response, Json::FORCE_ARRAY);
    }

    /**
     * @return mixed[]
     */
    public function get(string $url, bool $useCache = true): array
    {
        if ($useCache && isset($this->cache[$url])) {
            return $this->cache[$url];
        }

        $this->cache[$url] = Json::decode(
            $this->createRequest('GET', $url, null)->getBody()->getContents(),
            Json::FORCE_ARRAY,
        );

        return $this->cache[$url];
    }

    private function createRequest(
        string $method,
        string $uri,
        ?string $body = null,
    ): ResponseInterface {
        $request = $this->requestFactory->createRequest($method, $this->baseUrl . $uri)
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', 'application/json')
            ->withHeader('X-API-TOKEN', $this->apiKey);

        if ($body !== null) {
            $request = $request->withBody($this->streamFactory->createStream($body));
        }

        $response = $this->httpClient->sendRequest($request);

        if ($response->getStatusCode() !== 200) {
            throw new EnergokalkulatorException($response->getBody()->getContents());
        }

        return $response;
    }
}
