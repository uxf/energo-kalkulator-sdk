<?php

declare(strict_types=1);

namespace EnergoKalkulator\Client;

use EnergoKalkulator\EnergoKalkulatorService;
use EnergoKalkulator\Exception\CalculationCombinationNotFoundException;
use EnergoKalkulator\Http\Request\Electricity\ElectricityCalculatorIndividualRequestBody;
use EnergoKalkulator\Http\Request\Electricity\ElectricityCalculatorRequest;
use EnergoKalkulator\Http\Request\Electricity\ElectricityEstimateWithoutPrepaidRequest;
use EnergoKalkulator\Http\Request\Electricity\ElectricityEstimateWithPrepaidRequest;
use EnergoKalkulator\Http\Request\Electricity\PriceProductPreviewListRequestBody;
use EnergoKalkulator\Http\Request\Gas\GasCalculatorIndividualRequestBody;
use EnergoKalkulator\Http\Request\Gas\GasCalculatorRequest;
use EnergoKalkulator\Http\Request\Gas\GasEstimateWithoutPrepaidRequest;
use EnergoKalkulator\Http\Request\Gas\GasEstimateWithPrepaidRequest;
use EnergoKalkulator\Http\Request\Gas\GasPriceProductPreviewListRequestBody;
use EnergoKalkulator\Http\Response\Electricity\BreakerResponse;
use EnergoKalkulator\Http\Response\Electricity\DistributionRateResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityCalculatorResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityPartnerResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityPriceProductResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityProductDetailResponse;
use EnergoKalkulator\Http\Response\Electricity\ElectricityProductResponse;
use EnergoKalkulator\Http\Response\Energo\EnergoPartnerResponse;
use EnergoKalkulator\Http\Response\Gas\GasCalculatorResponse;
use EnergoKalkulator\Http\Response\Gas\GasPartnerResponse;
use EnergoKalkulator\Http\Response\Gas\GasPriceProductResponse;
use EnergoKalkulator\Http\Response\Gas\GasProductDetailResponse;
use EnergoKalkulator\Http\Response\Gas\GasProductResponse;
use EnergoKalkulator\Http\Response\Shared\CodebookResponse;
use UXF\Hydrator\ObjectHydrator;

class HttpEnergoKalkulatorService implements EnergoKalkulatorService
{
    public function __construct(
        public readonly ObjectHydrator $objectHydrator,
        public readonly HttpClient $httpClient,
    ) {
    }

    /**
     * @return ElectricityPartnerResponse[]
     */
    public function getPartners(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/electrozone/partner'),
            ElectricityPartnerResponse::class,
        );
    }

    /**
     * @return GasPartnerResponse[]
     */
    public function getGasPartners(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/gaszone/partner'),
            GasPartnerResponse::class,
        );
    }

    /**
     * @return DistributionRateResponse[]
     */
    public function getDistributionRates(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/electrozone/distributionrate'),
            DistributionRateResponse::class,
        );
    }

    /**
     * @return CodebookResponse[]
     */
    public function getFloorAreaRanges(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/electrozone/estimate-floor-area'),
            CodebookResponse::class,
        );
    }

    /**
     * @return CodebookResponse[]
     */
    public function getPersonsCountRanges(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/electrozone/estimate-persons-counts'),
            CodebookResponse::class,
        );
    }

    /**
     * @return CodebookResponse[]
     */
    public function getIsolations(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/electrozone/estimate-isolation'),
            CodebookResponse::class,
        );
    }

    /**
     * @return BreakerResponse[]
     */
    public function getBreakers(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/electrozone/breaker'),
            BreakerResponse::class,
        );
    }

    /**
     * @return EnergoPartnerResponse[]
     */
    public function getEnergoPartners(): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/energozone/energopartner'),
            EnergoPartnerResponse::class,
        );
    }

    public function getEnergoPartner(int $id): EnergoPartnerResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->get("/api/app/energozone/energopartner/{$id}"),
            EnergoPartnerResponse::class,
        );
    }

    public function getAdminGasCalculationResult(
        GasCalculatorIndividualRequestBody | GasCalculatorRequest $input,
    ): GasCalculatorResponse {
        if ($input instanceof GasCalculatorIndividualRequestBody) {
            $response = $this->httpClient->post(
                '/api/app/gaszone/calculator/individual',
                $input,
            );
        } else {
            $response = $this->httpClient->post(
                '/api/app/gaszone/calculator',
                $input,
            );
        }

        if (isset($response['errorId']) && $response['errorId'] === 404) {
            throw new CalculationCombinationNotFoundException();
        }

        return $this->objectHydrator->hydrateArray($response, GasCalculatorResponse::class);
    }

    public function getAdminElectricityCalculationResult(
        ElectricityCalculatorIndividualRequestBody | ElectricityCalculatorRequest $input,
    ): ElectricityCalculatorResponse {
        if ($input instanceof ElectricityCalculatorIndividualRequestBody) {
            $response = $this->httpClient->post(
                '/api/app/electrozone/calculator/individual',
                $input,
            );
        } else {
            $response = $this->httpClient->post(
                '/api/app/electrozone/calculator',
                $input,
            );
        }

        if (isset($response['errorId']) && $response['errorId'] === 404) {
            throw new CalculationCombinationNotFoundException();
        }

        return $this->objectHydrator->hydrateArray($response, ElectricityCalculatorResponse::class);
    }

    public function getElectricityProduct(int $id): ElectricityProductDetailResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->get("/api/app/electrozone/product/{$id}"),
            ElectricityProductDetailResponse::class,
        );
    }

    /**
     * @return ElectricityProductResponse[]
     */
    public function getElectricityProducts(int $partnerId): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/electrozone/partner/' . $partnerId . '/products'),
            ElectricityProductResponse::class,
        );
    }

    /**
     * @return GasProductResponse[]
     */
    public function getGasProducts(int $partnerId): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->get('/api/app/gaszone/partner/' . $partnerId . '/products'),
            GasProductResponse::class,
        );
    }

    public function getGasProduct(int $id): GasProductDetailResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->get("/api/app/gaszone/product/{$id}"),
            GasProductDetailResponse::class,
        );
    }

    /**
     * @return ElectricityPriceProductResponse[]
     */
    public function getElectricityPricePreview(PriceProductPreviewListRequestBody $input): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->post(
                '/api/app/electrozone/priceproduct-preview-list',
                $input,
            ),
            ElectricityPriceProductResponse::class,
        );
    }

    /**
     * @return GasPriceProductResponse[]
     */
    public function getGasPricePreview(GasPriceProductPreviewListRequestBody $input): array
    {
        return $this->objectHydrator->hydrateArrays(
            $this->httpClient->post(
                '/api/app/gaszone/priceproduct-preview-list',
                $input,
            ),
            GasPriceProductResponse::class,
        );
    }

    public function getElectricityCalculationIndividual(
        ElectricityCalculatorIndividualRequestBody $input,
    ): ElectricityCalculatorResponse {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/electrozone/calculator/individual',
                $input,
            ),
            ElectricityCalculatorResponse::class,
        );
    }

    public function getElectricityCalculation(ElectricityCalculatorRequest $input): ElectricityCalculatorResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/electrozone/calculator',
                $input,
            ),
            ElectricityCalculatorResponse::class,
        );
    }

    public function getElectricityEstimationWithPrepaid(
        ElectricityEstimateWithPrepaidRequest $input,
    ): ElectricityCalculatorResponse {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/electrozone/estimate-calculate-with-prepaid',
                $input,
            ),
            ElectricityCalculatorResponse::class,
        );
    }

    public function getElectricityEstimationWithoutPrepaid(
        ElectricityEstimateWithoutPrepaidRequest $input,
    ): ElectricityCalculatorResponse {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/electrozone/estimate-calculate-without-prepaid',
                $input,
            ),
            ElectricityCalculatorResponse::class,
        );
    }

    public function getGasCalculationIndividual(GasCalculatorIndividualRequestBody $input): GasCalculatorResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/gaszone/calculator/individual',
                $input,
            ),
            GasCalculatorResponse::class,
        );
    }

    public function getGasCalculation(GasCalculatorRequest $input): GasCalculatorResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/gaszone/calculator',
                $input,
            ),
            GasCalculatorResponse::class,
        );
    }

    public function getGasEstimateWithPrepaid(GasEstimateWithPrepaidRequest $input): GasCalculatorResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/gaszone/estimate-calculate-with-prepaid',
                $input,
            ),
            GasCalculatorResponse::class,
        );
    }

    public function getGasEstimateWithoutPrepaid(GasEstimateWithoutPrepaidRequest $input): GasCalculatorResponse
    {
        return $this->objectHydrator->hydrateArray(
            $this->httpClient->post(
                '/api/app/gaszone/estimate-calculate-without-prepaid',
                $input,
            ),
            GasCalculatorResponse::class,
        );
    }
}
