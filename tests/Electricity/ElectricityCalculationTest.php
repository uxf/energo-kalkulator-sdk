<?php

declare(strict_types=1);

namespace EnergoKalkulatorTests\Electricity;

use EnergoKalkulator\Http\Request\Electricity\ElectricityCalculatorIndividualRequestBody;
use EnergoKalkulator\Http\Request\Electricity\ElectricityCalculatorRequest;
use EnergoKalkulator\Http\Request\Electricity\ElectricityEstimateWithoutPrepaidRequest;
use EnergoKalkulator\Http\Request\Electricity\ElectricityEstimateWithPrepaidRequest;
use EnergoKalkulatorTests\StoryTestCase;

class ElectricityCalculationTest extends StoryTestCase
{
    public function testCalculationIndividual(): void
    {
        $service = $this->getService();
        $response = $service->getAdminElectricityCalculationResult(new ElectricityCalculatorIndividualRequestBody(
            individualPriceExpectedMonthly: 111,
            individualPriceExpectedMWhHighTariff: 222,
            individualPriceExpectedMWhLowTariff: 0,
            distributionRate: 3,
            breaker: 5,
            consumptionHighTariff: 3333,
            consumptionLowTariff: 0,
            distributor: 1,
            lastInvoicedPrice: 23323,
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }

    public function testCalculation(): void
    {
        $service = $this->getService();
        $response = $service->getAdminElectricityCalculationResult(new ElectricityCalculatorRequest(
            distributionRate: 6,
            breaker: 8,
            consumptionHighTariff: 3333,
            consumptionLowTariff: 2222,
            distributor: 2,
            lastInvoicedPrice: 0,
            currentPartner: 213,
            currentProductName: 'Elektřina smlouva na 3 roky (smlouva od 1.10.2020)',
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }

    public function testEstimateCalculationWithoutPrepaidStoryTest(): void
    {
        $service = $this->getService();
        $response = $service->getElectricityEstimationWithoutPrepaid(new ElectricityEstimateWithoutPrepaidRequest(
            buildingType: 2,
            personsCount: 3,
            floorArea: 1,
            isolation: 2,
            distributor: 3,
            usageCommon: true,
            usageWaterHeating: true,
            usageHeating: true,
            currentPartner: 201,
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }

    public function testEstimateCalculationWithPrepaidStoryTest(): void
    {
        $service = $this->getService();
        $response = $service->getElectricityEstimationWithPrepaid(new ElectricityEstimateWithPrepaidRequest(
            buildingType: 2,
            distributor: 2,
            prepaid: 1500.0,
            usageCommon: false,
            usageWaterHeating: false,
            usageHeating: true,
            currentPartner: 210,
            currentProduct: 22,
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }
}
