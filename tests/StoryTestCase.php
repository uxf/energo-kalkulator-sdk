<?php

declare(strict_types=1);

namespace EnergoKalkulatorTests;

use EnergoKalkulator\EnergoKalkulatorService;
use Nette\Utils\Random;
use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\Core\Test\JsonDriver;
use function Safe\json_decode;
use function Safe\json_encode;

abstract class StoryTestCase extends KernelTestCase
{
    use MatchesSnapshots;

    /** @var bool */
    protected $cleanup = true;

    /** @var string */
    private $sqlitePath = '';

    protected function setUp(): void
    {
        // dump(get_class($this));//exit;
        parent::setUp();
        // dump(get_class($this));
        $this->sqlitePath = __DIR__ . '/../../tests/uxf-base-backend_' . Random::generate() . '.sqlite';
        copy(__DIR__ . '/../../tests/uxf-base-backend.sqlite', $this->sqlitePath);
        putenv('TEST_DATABASE_URL=' . $this->sqlitePath);
    }

    protected function tearDown(): void
    {
        if ($this->cleanup) {
            unlink($this->sqlitePath);
        }

        parent::tearDown();
    }

    /**
     * @param string[] $additionalDangerousKeys
     */
    public function assertSnapshot(
        mixed $actual,
        bool $clearDangerousValues = true,
        array $additionalDangerousKeys = [],
    ): void {
        if (is_object($actual)) {
            $actual = json_decode(json_encode($actual), true);
        }

        $this->assertMatchesSnapshot(
            $clearDangerousValues ? $this->clearDangerousValues($actual, $additionalDangerousKeys) : $actual,
            new JsonDriver(),
        );
    }

    public function getService(): EnergoKalkulatorService
    {
        $provider = self::getContainer()->get(SmokeServiceProvider::class);
        assert($provider instanceof SmokeServiceProvider);
        $service = $provider->service;
        return $service;
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    /**
     * @param mixed $data
     * @param string[] $additionalDangerousKeys
     * @return mixed
     */
    private function clearDangerousValues($data, array $additionalDangerousKeys = [])
    {
        $securedData = [];

        if (!is_array($data)) {
            return $data;
        }

        foreach ($data as $key => $value) {
            if (in_array($key, ['id', 'uuid', 'createdAt', 'completedAt', ...$additionalDangerousKeys], true)) {
                $securedData[$key] = '=== dangerous value for snapshot ===';
            } else {
                $securedData[$key] = $this->clearDangerousValues($value, $additionalDangerousKeys);
            }
        }

        return $securedData;
    }
}
