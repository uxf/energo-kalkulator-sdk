<?php

declare(strict_types=1);

namespace EnergoKalkulatorTests;

use EnergoKalkulator\EnergoKalkulatorBundle;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use UXF\Core\UXFCoreBundle;
use UXF\Hydrator\UXFHydratorBundle;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): iterable
    {
        yield new FrameworkBundle();
        yield new UXFCoreBundle();
        yield new UXFHydratorBundle();
        yield new EnergoKalkulatorBundle();
    }

    public function getCacheDir(): string
    {
        return __DIR__ . '/../var/cache/test-sdk';
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->extension('framework', [
            'test' => true,
        ]);

        $container->extension('energo_kalkulator', [
            'base_url' => 'https://energokalkulator.uxf.dev',
            'api_key' => 'test-api-token',
        ]);

        $services = $container->services();
        $services->set(SmokeServiceProvider::class)->autowire()->public();

        $services->alias(RequestFactoryInterface::class, 'psr18.http_client');
        $services->alias(StreamFactoryInterface::class, 'nyholm.psr7.psr17_factory');

        $services->set(ClientInterface::class, TestHttpClient::class);
        $services->set('nyholm.psr7.psr17_factory', Psr17Factory::class);
    }
}
