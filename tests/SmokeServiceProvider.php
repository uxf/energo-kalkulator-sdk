<?php

declare(strict_types=1);

namespace EnergoKalkulatorTests;

use EnergoKalkulator\EnergoKalkulatorService;

final readonly class SmokeServiceProvider
{
    public function __construct(
        public EnergoKalkulatorService $service,
    ) {
    }
}
