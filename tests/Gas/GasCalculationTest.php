<?php

declare(strict_types=1);

namespace EnergoKalkulatorTests\Gas;

use App\Tests\GasZone\Story\CalculationStoryTest;
use EnergoKalkulator\Http\Request\Gas\GasCalculatorIndividualRequestBody;
use EnergoKalkulator\Http\Request\Gas\GasCalculatorRequest;
use EnergoKalkulator\Http\Request\Gas\GasEstimateWithoutPrepaidRequest;
use EnergoKalkulator\Http\Request\Gas\GasEstimateWithPrepaidRequest;
use EnergoKalkulatorTests\StoryTestCase;

class GasCalculationTest extends StoryTestCase
{
    public const ADDRESS_1 = 21810150; // Údolní 1317/63, Praha, Braník

    public function testCalculationIndividual(): void
    {
        $service = $this->getService();
        $response = $service->getAdminGasCalculationResult(new GasCalculatorIndividualRequestBody(
            individualPriceExpectedMonthly: 64.13,
            individualPriceExpectedMWh: 854.26,
            consumptionMWh: 3,
            smartAddressId: self::ADDRESS_1,
            lastInvoicedPrice: 33333,
            currentProduct: 1,
            currentPartner: 4,
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }

    public function testCalculation(): void
    {
        $service = $this->getService();
        $response = $service->getAdminGasCalculationResult(new GasCalculatorRequest(
            consumptionMWh: 0.342123321872778237737372651,
            distributor: 86,
            lastInvoicedPrice: 33333,
            currentProduct: 4,
            currentPartner: 1,
            forceDiscountToInvoice: 5,
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }

    public function testEstimateCalculationWithoutPrepaidStoryTest(): void
    {
        $service = $this->getService();
        $response = $service->getGasEstimateWithoutPrepaid(new GasEstimateWithoutPrepaidRequest(
            buildingType: 1,
            personsCount: 1,
            floorArea: 1,
            isolation: 1,
            smartAddressId: CalculationStoryTest::ADDRESS_1,
            usageCommon: true,
            currentProduct: 1,
            currentPartner: 1,
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }

    public function testEstimateCalculationWithPrepaidStoryTest(): void
    {
        $service = $this->getService();
        $response = $service->getGasEstimateWithPrepaid(new GasEstimateWithPrepaidRequest(
            distributor: 85,
            prepaid: 3333,
            currentProduct: 1,
            currentPartner: 1,
        ));

        $this->assertSnapshot($response->calculatedItems[0]);
        $this->assertSnapshot($response->common);
    }
}
