<?php

declare(strict_types=1);

namespace EnergoKalkulatorTests;

use GuzzleHttp\Psr7\ServerRequest;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;

class TestHttpClient implements ClientInterface
{
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $httpFoundationFactory = new HttpFoundationFactory();
        $kernel = new \App\Kernel('test', true);
        $request = $httpFoundationFactory->createRequest(
            new ServerRequest($request->getMethod(), $request->getUri(), $request->getHeaders(), $request->getBody()->getContents()),
        );
        $response = $kernel->handle($request);

        $psr17Factory = new Psr17Factory();
        $psrHttpFactory = new PsrHttpFactory($psr17Factory, $psr17Factory, $psr17Factory, $psr17Factory);
        $psrResponse = $psrHttpFactory->createResponse($response);
        $psrResponse->getBody()->rewind();
        return $psrResponse;
    }
}
